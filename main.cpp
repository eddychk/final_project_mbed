#include "mbed.h"
#include "pindef.h"
#include "TextLCD.h"

Mutex mutex;
Thread thread;

// Buttons
DigitalIn modeButton(D4);
DigitalIn setButton(D5);

// Temperature Sensor
I2C tempSensor(I2C_SDA, I2C_SCL);
const int temp_addr = 0x90;
char cmd[] = {0x51, 0xAA};
char read_temp[2];

// LCD
TextLCD lcd(A5, A4, A0, A1, A2, A3);

void print_temperature()
{
    while(1){
        // Send commands to get the data from the temperature sensor
        tempSensor.write(temp_addr, &cmd[0], 1);
        wait(1);
        tempSensor.write(temp_addr, &cmd[1], 1);
        wait(1);
        tempSensor.read(temp_addr, read_temp, 2);
        wait(1);
        
        // Convert temperature to Celsius
        float temp = (float((read_temp[0] << 8) | read_temp[1]) / 256);

        // Print temperature to the serial monitor
        mutex.lock();
        lcd.locate(9, 0);
        lcd.printf("%.2f C", temp);
        mutex.unlock();
    }
}

void set_clock(struct tm *time)
{
    int tmpHour = 0;
    int tmpMin = 0;
    lcd.locate(0, 0);
    lcd.printf("Set Time: %.2d:%.2d", tmpHour, tmpMin);
    lcd.locate(0, 1);
    lcd.printf("SWITCH       SET");

    while (1)
    {   
        lcd.locate(0, 0);
        lcd.printf("Set Time: %.2d:%.2d", tmpHour, tmpMin);
        if (!setButton) {
            tmpHour = (tmpHour + 1) % 24;
            wait(0.25);
            };
        if (!modeButton)
            break;
    };
    wait(0.5);
    lcd.locate(0 ,1);
    lcd.printf("DONE         SET");
    while (1)
    {   
        lcd.locate(0, 0);
        lcd.printf("Set Time: %.2d:%.2d", tmpHour, tmpMin);
        if (!setButton) {
            tmpMin = (tmpMin + 1) % 60;
            wait(0.25);
            };
        if (!modeButton)
            break;
    };
    time->tm_hour = tmpHour;
    time->tm_min = tmpMin;
}

int main()
{
    int delay;
    clock_t s, n;
    struct tm timeinfo;
    int seconds = 0;

    set_clock(&timeinfo);
    delay = (timeinfo.tm_hour * 3600) + (timeinfo.tm_min * 60);
    s = clock();
    lcd.cls();
    // Start thread
    thread.start(print_temperature);
    
    // Blink the leds all together
    while (1) {
        n = clock();
        mutex.lock();
        lcd.locate(0, 1);
        seconds = ((n / CLOCKS_PER_SEC) - (s / CLOCKS_PER_SEC) + delay);
        lcd.printf("%02d:%02d:%02d ",(seconds / 3600) % 24, (seconds / 60) % 60, seconds % 60);
        mutex.unlock();
    }   
}
